data "aws_iam_role" "existing_ssm_role" {
  name = "ec2-ssm-role"
}


resource "aws_instance" "sftp_instance" {
  ami             = var.ami_id
  #ami            = "${var.ami_id}"
  instance_type = var.instance_type
  key_name      = var.key_name
  iam_instance_profile = data.aws_iam_role.existing_ssm_role.name
  subnet_id =  var.subnet_id
  vpc_security_group_ids = var.security_group_ids
  #security_group_ids = var.security_group_ids 


  user_data = <<-EOF
#!/bin/bash

# Define variables
SFTP_USERNAME="sasender"
SFTP_PASSWORD="sasender"

# Create SFTP user
#sudo adduser --disabled-password --gecos "" $SFTP_USERNAME

#sudo adduser --gecos "" $SFTP_USERNAME

sudo useradd -m $SFTP_USERNAME



# Set the SFTP user's password
echo "$SFTP_USERNAME:$SFTP_PASSWORD" | sudo chpasswd

sudo mkdir -p /var/sftp/myfolder/data/

sudo chown root:root /var/sftp/myfolder

sudo chown sasender:sasender /var/sftp/myfolder/data/

sudo chmod 777 /var/sftp/myfolder/data

# Configure SSH for SFTP-only access
sudo bash -c "cat >> /etc/ssh/sshd_config <<EOL

Match User $SFTP_USERNAME
    ForceCommand internal-sftp
    PasswordAuthentication yes
    ChrootDirectory /var/sftp/myfolder
    PermitRootLogin no
    AllowTcpForwarding no
    X11Forwarding no
EOL"

# Restart SSH service
#sudo service ssh restart
sudo systemctl restart sshd

# Set correct permissions
sudo chown root:root /home/$SFTP_USERNAME
#sudo chmod 755 /home/$SFTP_USERNAME

echo "SFTP user $SFTP_USERNAME has been set up successfully."
  EOF
}
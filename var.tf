variable "ami_id" {
  type = string
  default = "null"
}

variable "key_name" {

  default = null
}

variable "instance_type" {

  default = null

}
variable "region" {

    default = null

}

variable "subnet_id" {
    default = null
}

variable "vpc_security_group_ids" {
  default = null

}

variable "security_group_ids" {
  description = "List of existing security group IDs"
  default  = null
}
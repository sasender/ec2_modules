provider "aws" {
  region = "ap-southeast-1"  # Set your desired AWS region
}

data "aws_iam_role" "existing_ssm_role" {
  name = "ec2-ssm-role"
}

module "sftp_instance" {
  source         = "./ec2_modules/sftp-user"
  ami_id         = var.ami_id  # Replace with the desired AMI ID
  instance_type  = var.instance_type               # Replace with the desired instance type
  key_name       = var.key_name     # Replace with the name of your key pair
  sftp_username  = "sftpuser"
  subnet_id = var.subnet_id
  #vpc_security_group_ids = ["sg-0bd6bcaac90780a4f"]
  security_group_ids    = ["sg-0bd6bcaac90780a4f"]
}
